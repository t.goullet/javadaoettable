package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.City;

public class DAOCity {
	
	private Connection cnx;
	 

	
	public DAOCity(Connection cnx) {
		this.cnx = cnx;
	}
	
	
	public City find (int id) throws SQLException {
		
		City city = null;
		String SQL = "SELECT * FROM city WHERE City_Id= ?";
		PreparedStatement ps = cnx.prepareStatement(SQL);
		ps.setInt(1, id);
		ResultSet rs  = ps.executeQuery();
		
		if (rs.next()) {
		city = new City().setCity_Id(rs.getInt("city_Id")).setName(rs.getString("name")).setCountryCode(rs.getString("countryCode")).setDistrict(rs.getString("district")).setPopulation(rs.getInt("population"));
		}
		return city;

	}
		
		
			
	
	
	public void saveOrUpdate(City c) throws SQLException {
		String SQL = "UPDATE city SET Name=?, CountryCode=?, District=?, Population=? WHERE City_Id=?";
		PreparedStatement ps = cnx.prepareStatement(SQL);
		ps.setString(1, c.getName());
		ps.setString(2, c.getCountryCode());
		ps.setString(3, c.getDistrict());
		ps.setInt(4, c.getPopulation());
		ps.setInt(5, c.getCity_Id());
		ps.executeQuery();
		
	}
	

	public void delete (int id) throws SQLException{
		String SQL = "DELETE FROM city WHERE City_Id =?";
		PreparedStatement ps = cnx.prepareStatement(SQL);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
    }
	
	public List<City> findAll() throws SQLException{
		String SQL = "SELECT * FROM city";
		PreparedStatement ps = cnx.prepareStatement(SQL);
		ResultSet rs = ps.executeQuery();
		List<City> cities = new ArrayList<>();

		while (rs.next()) {
			City city = new City().setCity_Id(rs.getInt("city_Id")).setName(rs.getString("name"))
					.setCountryCode(rs.getString("countryCode")).setDistrict(rs.getString("district"))
					.setPopulation(rs.getInt("population"));
			cities.add(city);
		}
		return cities;
	}
	
	
	public int count() throws SQLException {
		int count = 0;
		String SQL = "COUNT (City_Id) FROM city";
		PreparedStatement ps = cnx.prepareStatement(SQL);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			 count = (rs.getInt(1));
		}
		return count;
		
	}
	
	public List<City> findCitiesFromCountry(String country) throws SQLException{
		
		String SQL = "SELECT * FROM city INNER JOIN country ON code=countryCode WHERE country.NAME LIKE ?";
		PreparedStatement ps = cnx.prepareStatement(SQL);
		ps.setString(1, country + "%");
		List<City> cities = findCitiesFromCountry( ps);
		return cities;
	
	}
	

	
	public List<City> findCitiesFromCountry(int country_id) throws SQLException{
		String SQL = "SELECT * FROM city INNER JOIN country ON code=countryCode WHERE country.Country_Id = ?";
		PreparedStatement ps = cnx.prepareStatement(SQL);
		ps.setInt(1, country_id);
		List<City> cities = findCitiesFromCountry( ps);
		return cities;
	}
		
	
	private List<City> findCitiesFromCountry (PreparedStatement ps) throws SQLException {
		
		ResultSet rs = ps.executeQuery();
		List<City> cities = new ArrayList<>();

		while (rs.next()) {
			City city = new City().setCity_Id(rs.getInt("city_Id")).setName(rs.getString("name"))
					.setCountryCode(rs.getString("countryCode")).setDistrict(rs.getString("district"))
					.setPopulation(rs.getInt("population"));
			cities.add(city);
		}
		return cities;
	}
	
	
	public List<City> findCitiesByName(String name) throws SQLException {

		String SQL = "SELECT * FROM city WHERE Name LIKE ?";
		PreparedStatement ps = cnx.prepareStatement(SQL);
		ps.setString(1, name + "%");
		ResultSet rs = ps.executeQuery();
		List<City> cities = new ArrayList<>();
		
		while(rs.next()) {
			City city = new City().setCity_Id(rs.getInt("City_Id")).setName(rs.getString("Name"))
					.setCountryCode(rs.getString("CountryCode")).setDistrict(rs.getString("District"))
					.setPopulation(rs.getInt("Population"));
			cities.add(city);
		}
		return cities;
	}
	
	
	

	
	

	
	
	
	
	
	
	
	
}
