import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SingletonDataBase {

	public Connection cnx;
	private String url;
	private String username;
	private String password;
	
	private static SingletonDataBase instance;
	
	
	private SingletonDataBase() throws SQLException {	
		this.url = SingletonConfigReader.getInstance().getValue("mariadb", "url");
		this.username = SingletonConfigReader.getInstance().getValue("mariadb", "username");
		this.password = SingletonConfigReader.getInstance().getValue("mariadb", "password");
		
		this.cnx = DriverManager.getConnection(url,username,password);
	}
	
	
	public Connection getCnx() {
		return cnx;
	}
	
	
	public static SingletonDataBase getInstance() throws SQLException {
		if(instance == null) {
			instance = new SingletonDataBase();			
		} else if (instance.getCnx().isClosed()) {
			instance = new SingletonDataBase();
		}
		return instance;
	}
}