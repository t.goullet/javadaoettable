package entities;

public class City {
	 
	int City_Id;
	String Name;
	String CountryCode;
	String District;
	int Population;
	
	
	public City() {
		
	}


	public City(int city_Id, String name, String countryCode, String district, int population) {
		City_Id = city_Id;
		Name = name;
		CountryCode = countryCode;
		District = district;
		Population = population;
	}


	public int getCity_Id() {
		return City_Id;
	}


	public City setCity_Id(int city_Id) {
		this.City_Id = city_Id;
		return this;
	}


	public String getName() {
		return Name;
	}


	public City setName(String name) {
		this.Name = name;
		return this;
	}


	public String getCountryCode() {
		return CountryCode;
	}


	public City setCountryCode(String countryCode) {
		this.CountryCode = countryCode;
		return this;
	}


	public String getDistrict() {
		return District;
	}


	public City setDistrict(String district) {
		this.District = district;
		return this;
	}


	public int getPopulation() {
		return Population;
	}


	public City setPopulation(int population) {
		this.Population = population;
		return this;
	}


	@Override
	public String toString() {
		return "City [City_Id=" + City_Id + ", Name=" + Name + "]";
	}
	
	

	
	
	
}
