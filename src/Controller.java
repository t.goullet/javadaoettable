
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import java.util.jar.Attributes.Name;

import javax.swing.JButton;

import DAO.DAOCity;
import entities.City;


public class Controller implements ActionListener{
	
	Win01 fenetre;
	DAOCity daocity;
	List<City>cities;
	int indexCity;
	
	
	public Controller(Win01 fenetre, DAOCity daocity) {
		
		this.fenetre = fenetre;
		this.daocity = daocity;
		

		fenetre.getBtnPrevious().addActionListener(this);
		fenetre.getBtnUpdate().addActionListener(this);
		fenetre.getBtnDelete().addActionListener(this);
		fenetre.getBtnNext().addActionListener(this);
		fenetre.getBtnSearch().addActionListener(this);
	}

	
	public void init() {
		try {
			cities = daocity.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		fenetre.getBtnPrevious().setEnabled(false);
		indexCity = 0;
		fenetre.setVisible(true);
		resfreshWindows(cities.get(indexCity));
		
	}
	
	

	private void doNextJob() {
		indexCity++;
		City city = cities.get(indexCity);
		resfreshWindows(city);
	}
	
	private void doPreviousJob() {
		indexCity--;
		resfreshWindows(cities.get(indexCity));
	}
	
	private void doUpdateJob() throws SQLException  {
		
		City city = cities.get(indexCity);
		city.setName(fenetre.getTxtNom().getText());
		city.setDistrict(fenetre.getTxtDistrict().getText());
		city.setCountryCode(fenetre.getTxtCode().getText());
		city.setPopulation(Integer.parseInt(fenetre.getTxtPopulation().getText()));
		daocity.saveOrUpdate(city);
		
		System.out.println("City updated");

	}
	
	
	private void doDeleteJob() {
		City city = cities.get(indexCity);
		try {
			daocity.delete(city.getCity_Id());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		if(cities.remove(city)) {
			System.out.println("City Deleted");
		}
		resfreshWindows(cities.get(indexCity));
	}
	
	
	public void doSearchJob() {
		City city = cities.get(indexCity);
		try {
			cities = daocity.findCitiesByName(fenetre.getTxtSearch().getText());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		resfreshWindows(cities.get(indexCity));
	}
	




private void resfreshWindows(City city) {
	if (indexCity == 0) {
		fenetre.getBtnPrevious().setEnabled(false);
	} else {
		fenetre.getBtnPrevious().setEnabled(true);
	}
	fenetre.getTxtId().setText("" + city.getCity_Id());
	fenetre.getTxtNom().setText(city.getName());
	fenetre.getTxtCode().setText(city.getCountryCode());
	fenetre.getTxtDistrict().setText(city.getDistrict());
	fenetre.getTxtPopulation().setText("" + city.getPopulation());
	fenetre.setVisible(true);
	
	fenetre.getProgressBar().setValue((indexCity+1)*100/cities.size());
	int percent = (indexCity+1)*100/cities.size();
	if(percent>99) {
		fenetre.getBtnNext().setEnabled(false);
	} else {
		fenetre.getBtnNext().setEnabled(true);
	}
	
		
}

@Override
public void actionPerformed(ActionEvent e) {
	JButton btn = (JButton)e.getSource();
	switch (btn.getName()) {
	case "Update":
		try {
			doUpdateJob();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	break;
	
	case "Previous":
		doPreviousJob();
		break;
	
	case "Next":
		doNextJob();
		break;
		
	case "Delete":
		doDeleteJob();
		break;
		
	case "Search":
		doSearchJob();
		break;
		
	default:
		break;
	}
	
}

}






