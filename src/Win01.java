import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JProgressBar;

public class Win01 extends JFrame {

	private JPanel contentPane;
	private JTextField txtId;
	private JTextField txtNom;
	private JTextField txtCode;
	private JTextField txtDistrict;
	private JTextField txtPopulation;
	private JTextField txtSearch;
	private JButton btnSearch;
	private JButton btnPrevious;
	private JButton btnUpdate;
	private JButton btnDelete;
	private JButton btnNext;
	private JProgressBar progressBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Win01 frame = new Win01();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Win01() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 677, 704);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAffichageDesVilles = new JLabel("Affichage des villes");
		lblAffichageDesVilles.setBounds(232, 26, 225, 15);
		contentPane.add(lblAffichageDesVilles);
		
		JLabel lblNewLabel = new JLabel("Id :");
		lblNewLabel.setBounds(29, 109, 70, 15);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nom :");
		lblNewLabel_1.setBounds(29, 136, 70, 15);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Code :");
		lblNewLabel_2.setBounds(29, 163, 70, 15);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("District :");
		lblNewLabel_3.setBounds(29, 190, 70, 15);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Population :");
		lblNewLabel_4.setBounds(29, 217, 96, 15);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblFindCitiesStarting = new JLabel("Find Cities starting with");
		lblFindCitiesStarting.setBounds(29, 276, 182, 15);
		contentPane.add(lblFindCitiesStarting);
		
		txtId = new JTextField();
		txtId.setEditable(false);
		txtId.setBounds(205, 109, 389, 19);
		contentPane.add(txtId);
		txtId.setColumns(10);
		
		txtNom = new JTextField();
		txtNom.setBounds(205, 136, 389, 19);
		contentPane.add(txtNom);
		txtNom.setColumns(10);
		
		txtCode = new JTextField();
		txtCode.setBounds(205, 163, 389, 19);
		contentPane.add(txtCode);
		txtCode.setColumns(10);
		
		txtDistrict = new JTextField();
		txtDistrict.setBounds(205, 190, 389, 19);
		contentPane.add(txtDistrict);
		txtDistrict.setColumns(10);
		
		txtPopulation = new JTextField();
		txtPopulation.setBounds(205, 217, 389, 19);
		contentPane.add(txtPopulation);
		txtPopulation.setColumns(10);
		
		txtSearch = new JTextField();
		txtSearch.setBounds(205, 274, 304, 19);
		contentPane.add(txtSearch);
		txtSearch.setColumns(10);
		
		btnSearch = new JButton("Search");
		btnSearch.setName("Search");
		btnSearch.setBounds(521, 271, 117, 25);
		contentPane.add(btnSearch);
		
		btnPrevious = new JButton("<<<");
		btnPrevious.setName("Previous");
		btnPrevious.setBounds(93, 633, 117, 25);
		contentPane.add(btnPrevious);
		
		btnUpdate = new JButton("Update");
		btnUpdate.setName("Update");
		btnUpdate.setBounds(223, 633, 117, 25);
		contentPane.add(btnUpdate);
		
		btnDelete = new JButton("Delete");
		btnDelete.setName("Delete");
		btnDelete.setBounds(352, 633, 117, 25);
		contentPane.add(btnDelete);
		
		btnNext = new JButton(">>>");
		btnNext.setName("Next");
		btnNext.setBounds(481, 633, 117, 25);
		contentPane.add(btnNext);
		
		progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setBounds(205, 303, 304, 14);
		contentPane.add(progressBar);
	}
	public JTextField getTxtId() {
		return txtId;
	}
	public JTextField getTxtNom() {
		return txtNom;
	}
	public JTextField getTxtCode() {
		return txtCode;
	}
	public JTextField getTxtDistrict() {
		return txtDistrict;
	}
	public JTextField getTxtPopulation() {
		return txtPopulation;
	}
	public JTextField getTxtSearch() {
		return txtSearch;
	}
	public JButton getBtnSearch() {
		return btnSearch;
	}
	public JButton getBtnPrevious() {
		return btnPrevious;
	}
	public JButton getBtnUpdate() {
		return btnUpdate;
	}
	public JButton getBtnDelete() {
		return btnDelete;
	}
	public JButton getBtnNext() {
		return btnNext;
	}
	public JProgressBar getProgressBar() {
		return progressBar;
	}
}
