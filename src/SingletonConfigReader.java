import java.io.File;
import java.io.IOException;
import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
 

public class SingletonConfigReader {

	private static String pathfile;
	public static Ini config;

	private static SingletonConfigReader instance = null;
	
	private SingletonConfigReader() {
		pathfile = "src/Config.ini";
		try {
			config = new Ini(new File(pathfile));
		} 
		catch (InvalidFileFormatException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static SingletonConfigReader getInstance() {
		if(instance == null) {
			instance = new SingletonConfigReader();
		}
		return instance;
	}
	
	public static String getValue(String section, String param) {
		if(section == null) {
			return null;
		}
		else {
			return config.get(section, param);
	}
	}


}